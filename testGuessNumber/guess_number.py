"""Test GuessNumber module"""
import random


def guess_int(start, stop):
    """guess_int function"""
    return random.randint(start, stop)


def guess_float(start, stop):
    """guess_float function"""
    return random.uniform(start, stop)
