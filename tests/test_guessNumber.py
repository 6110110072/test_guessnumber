import testGuessNumber.guess_number as guess_number
import unittest
from unittest import mock


class test_guess_number(unittest.TestCase):
    @mock.patch('random.randint', return_value=int(1))
    def test_guess_int_1_to_5_should_be_1(self, random_randint):
        result = guess_number.guess_int(1, 5)
        self.assertEqual(result, 1, "should be 1")
        random_randint.assert_called_once()
        random_randint.assert_called_with(1, 5)
        random_randint.assert_called_once_with(1, 5)

    @mock.patch('random.randint', return_value=int(40))
    def test_guess_int_22_to_55_should_be_40(self, random_randint):
        result = guess_number.guess_int(22, 55)
        self.assertEqual(result, 40, "should be 40")
        random_randint.assert_called_once()
        random_randint.assert_called_with(22, 55)

    @mock.patch('random.randint', return_value=int(62))
    def test_guess_int_1_to_99_should_be_62(self, random_randint):
        result = guess_number.guess_int(1, 99)
        self.assertEqual(result, 62, "should be 62")
        random_randint.assert_called_once()
        random_randint.assert_called_with(1, 99)

    @mock.patch('random.randint', return_value=int(77))
    def test_guess_int_21_to_77_should_be_77(self, random_randint):
        result = guess_number.guess_int(21, 77)
        self.assertEqual(result, 77, "should be 77")
        random_randint.assert_called_once()
        random_randint.assert_called_with(21, 77)

    @mock.patch('random.randint', return_value=int(-200))
    def test_guess_int_0_to_m400_should_be_m200(self, random_randint):
        result = guess_number.guess_int(0, -400)
        self.assertEqual(result, -200, "should be -200")
        random_randint.assert_called_once()
        random_randint.assert_called_with(0, -400)

    @mock.patch('random.randint', return_value=int(0))
    def test_guess_int_m100_to_100_should_be_0(self, random_randint):
        result = guess_number.guess_int(-100, 100)
        self.assertEqual(result, 0, "should be 0")
        random_randint.assert_called_once()
        random_randint.assert_called_with(-100, 100)

    ##########################################################################

    @mock.patch('random.uniform', return_value=float(2.22))
    def test_guess_float_1dot22_to_3_should_be_2dot22(self, random_randint):
        result = guess_number.guess_float(1.22, 3)
        self.assertEqual(result, 2.22, "should be 2.22")
        random_randint.assert_called_once()
        random_randint.assert_called_with(1.22, 3)

    @mock.patch('random.uniform', return_value=float(6.42))
    def test_guess_float_1_to_10_should_be_6dot42(self, random_randint):
        result = guess_number.guess_float(1, 10)
        self.assertEqual(result, 6.42, "should be 6.42")
        random_randint.assert_called_once()
        random_randint.assert_called_with(1, 10)

    @mock.patch('random.uniform', return_value=float(103.326))
    def test_guess_float_1dot11_to_999dot99_should_be_103dot326(self, random_randint):
        result = guess_number.guess_float(1.11, 999.99)
        self.assertEqual(result, 103.326, "should be 103.326")
        random_randint.assert_called_once()
        random_randint.assert_called_with(1.11, 999.99)

    @mock.patch('random.uniform', return_value=float(1024.123))
    def test_guess_float_9dot99_to_1111dot111_should_be_1024dot123(self, random_randint):
        result = guess_number.guess_float(9.99, 1111.111)
        self.assertEqual(result, 1024.123, "should be 1024.123")
        random_randint.assert_called_once()
        random_randint.assert_called_with(9.99, 1111.111)

    @mock.patch('random.uniform', return_value=float(-33.58))
    def test_guess_float_m30_to_m40_should_be_m33dot58(self, random_randint):
        result = guess_number.guess_float(-30, -40)
        self.assertEqual(result, -33.58, "should be -33.58")
        random_randint.assert_called_once()
        random_randint.assert_called_with(-30, -40)

    @mock.patch('random.uniform', return_value=float(0.12345))
    def test_guess_float_m1_to_1_should_be_0dot12345(self, random_randint):
        result = guess_number.guess_float(-1, 1)
        self.assertEqual(result, 0.12345, "should be 0.12345")
        random_randint.assert_called_once()
        random_randint.assert_called_with(-1, 1)
